let curr_temp = $("#temp");
let img = $("#icon");
let desc = $("#desc");
let name = $("#name");
let country = $("#country");
let feels = $("#feel");
let curr_notes = $("#note");
let wind = $("#wind");
let pressure = $("#pressure");
let humidity = $("#humidity");
let forecast_wrapper = $(".scrolling-wrapper");
var month = new Array(12);
month[0] = "January";
month[1] = "February";
month[2] = "March";
month[3] = "April";
month[4] = "May";
month[5] = "June";
month[6] = "July";
month[7] = "August";
month[8] = "Septempber";
month[9] = "October";
month[10] = "November";
month[11] = "December";
if (window.localStorage.theme == "light") {
  $("body").attr("class", "light");
  $("img.toggler").attr("src", "images/sun.svg");
  window.localStorage.theme = "light";
} else {
  $("body").attr("class", "dark");
  $("img.toggler").attr("src", "images/moon.svg");
  window.localStorage.theme = "dark";
}
$(document).ready(() => {
  // get users lat/long

  $("img.toggler").on({
    click: function () {
      var src =
        $(this).attr("src") === "images/moon.svg" ? "images/sun.svg" : "images/moon.svg";
      $(this).attr("src", src);
      // $(this).hide(700);

      if (src === "images/sun.svg") {
        $("body").attr("class", "light");
        window.localStorage.theme = "light";
      } else {
        $("body").attr("class", "dark");
        window.localStorage.theme = "dark";
      }
    },
  });
  var getPosition = {
    enableHighAccuracy: false,
    timeout: 9000,
    maximumAge: 0,
  };

  function success(gotPosition) {
    var uLat = gotPosition.coords.latitude;
    var uLon = gotPosition.coords.longitude;
    console.log(`${uLat}`, `${uLon}`);
    const settings = {
      async: true,
      crossDomain: true,
      url: `https://weatherapi-com.p.rapidapi.com/forecast.json?q=${uLat},${uLon}&days=3`,
      method: "GET",
      headers: {
        "x-rapidapi-key": "55e00f5629msha9991ea81532a93p18a9e0jsnc0f29c202338",
        "x-rapidapi-host": "weatherapi-com.p.rapidapi.com",
      },
    };

    $.ajax(settings).done(function (response) {
      console.log(response);
      let info = response.current;
      let location = response.location;
      let state = location.region;
      let region = location.country;
      let time = location.localtime.substr(10, location.localtime.length);
      let ctime = parseInt(time.substr(0, 3));
      let humidity_val = info.humidity;
      let wind_val = info.wind_kph;
      let pressure_val = info.pressure_in;
      let note;
      if (ctime > 5 && ctime < 12) note = "Morning";
      else if (ctime > 12 && ctime < 5) note = "Afternoon";
      else if (ctime >= 5 && ctime < 8) note = "Evening";
      else note = "Night";
      curr_notes.html(note);
      let description = info.condition.text;
      let temp = info.temp_c;
      let icon = info.condition.icon;
      feels.html(parseInt(info.feelslike_c));
      country.html(`, ${region}`);
      name.html(state);
      desc.html(description);
      img.attr("src", icon);

      curr_temp.html(parseInt(temp));
      pressure.html(`${pressure_val}<span>inHg</span>`);
      humidity.html(`${humidity_val}<span>%</span>`);
      wind.html(`${wind_val}<span>kph</span>`);

      //forecast daily
      let sunrise = response.forecast.forecastday[0].astro.sunrise;
      let sunset = response.forecast.forecastday[0].astro.sunset;
      $("#sunrise-time").html(sunrise);
      $("#sunset-time").html(sunset);
      let forecast = response.forecast.forecastday[0].hour,
        meridian = "AM",
        forecastToday = [],
        tempToday = [],
        feelsLike = [];
      forecast.forEach((el, index) => {
        let dtime = el.time.substr(10, 3);
        parseInt(dtime) >= 12 ? (meridian = "PM") : (meridian = "AM");
        if (parseInt(dtime) === 0) {
          dtime = 12;
        }
        if (parseInt(dtime) > 12) {
          dtime -= 12;
        }
        forecastToday.push(parseInt(dtime) + " " + meridian.toLocaleLowerCase());
        tempToday.push(el.temp_c);
        feelsLike.push(el.feelslike_c);
        forecast_wrapper.append(`
            <div class="card" id=${index}>
            <p class="time-forecast">${parseInt(dtime)} ${meridian}</p>
              <img src="${el.condition.icon}">
              <p class="temp-forecast">${el.temp_c}°C</p>
            </div>
        `);
      });
      var ctx = document.getElementById("myChart");

      var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: "line",

        // The data for our dataset
        data: {
          labels: forecastToday,
          datasets: [
            {
              label: "Temperature",
              backgroundColor: "#3c41c67e",
              borderColor: "#3c40c6",
              data: tempToday,
              pointRadius: 2,
              pointHoverRadius: 5,
            },
            // {
            //   label: "Real Feel",
            //   backgroundColor: "#ef540070",
            //   borderColor: "#ef5200",
            //   data: feelsLike,
            // },
          ],
        },

        options: {
          scales: {
            xAxes: [
              {
                gridLines: {
                  color: "rgba(0, 0, 0, 0)",
                },
                ticks: {
                  fontColor: "#999",
                },
                scaleLabel: {
                  display: true,
                  // labelString: "Time",
                  fontColor: "#999",
                },
              },
            ],
            yAxes: [
              {
                ticks: {
                  display: false,
                },
                gridLines: {
                  color: "rgba(0, 0, 0, 0)",
                },
                scaleLabel: {
                  display: true,
                  labelString: "Temperature (°C)",
                  fontColor: "#999",
                },
              },
            ],
          },
          animation: {
            easing: "easeInOutElastic",
            // easing: "easeInBounce",
            duration: 2000,
            tension: {
              duration: 1000,
              easing: "linear",
              from: 1,
              to: 0,
              loop: true,
            },
          },
          legend: {
            display: false,
          },
        },
      });
      let forecastThree = response.forecast.forecastday;
      console.log(forecastThree);
      forecastThree.forEach((e) => {
        let date = e.date;
        let dt = date.split(" ", 1);
        let today = dt[0].split("-").reverse().join("-");
        let day = parseInt(today.substr(0, 2));
        let index = parseInt(today.substr(3, 2));
        let mon = month[index - 1];
        let tempIcon = e.day.condition.icon;
        // alert(tempIcon);
        let tempMax = parseInt(e.day.maxtemp_c);
        let tempMin = parseInt(e.day.mintemp_c);
        console.log(mon, day, tempMax, tempMin);
        $("#forecast").append(`
          <tr>
            <td>${mon} ${day}</td>
            <td><img src="${tempIcon}" alt="" height="40" /></td>
            <td class="max">${tempMax}°C</td>
            <td class="min">${tempMin}°C</td>
          </tr>
        `);
      });

      window.location.href = `#${ctime}`;
      $(window).scrollTop(0);
    });
  }

  function error(err) {
    console.warn(`ERROR(${err.code}): ${err.message}`);
  }

  navigator.geolocation.getCurrentPosition(success, error, getPosition);
});
